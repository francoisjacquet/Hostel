��          �      \      �     �     �  	   �     �     �               !     *     1  (   J     s  	   x     �     �     �  .   �  3   �  &      �  '  
   �     �     �  
   �     �  	   �     �  	   �     	       )   ,     V  	   [     e     j     m  (   {  1   �      �                                                                      	             
                All Buildings Building Buildings Capacity Include Students My Room New Building New Room Remove Remove Inactive Students Remove selected students from their room Room Room List Rooms Since Student's Room The room was assigned to the selected student. The selected students were removed from their room. The student was removed from the room. Project-Id-Version: Hostel module for RosarioSIS
PO-Revision-Date: 2024-04-22 17:52+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-SearchPath-0: .
 Vse stavbe Stavba Stavbe Kapaciteta Vključi dijake Moja soba Nova stavba Nova soba Odstrani Odstrani neaktivne dijake Odstranite izbrane dijake iz njihove sobe Soba Izpis sob Sobe Od Dijakova soba Soba je bila dodeljena izbranemu dijaku. Izbrani dijaki so bili odstranjeni iz svoje sobe. Dijak je bil odstranjen iz sobe. 