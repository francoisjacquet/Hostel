��          �      l      �     �     �  	   �                    #     +     8     A     H  (   a     �  	   �     �     �     �  .   �  3   �  &     �  >     �     �  	   �  	   �  	   �                /     >     P  !   X  7   z     �     �     �     �     �  ;     3   >  -   r                               	                                      
                             All Buildings Building Buildings Capacity Hostel Include Students My Room New Building New Room Remove Remove Inactive Students Remove selected students from their room Room Room List Rooms Since Student's Room The room was assigned to the selected student. The selected students were removed from their room. The student was removed from the room. Project-Id-Version: Hostel module for RosarioSIS
PO-Revision-Date: 2024-04-22 17:51+0200
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-SearchPath-0: .
 Todos los Edificios Edificio Edificios Capacidad Internado Incluir los Estudiantes Mi Habitación Nuevo Edificio Nueva Habitación Retirar Retirar los Estudiantes Inactivos Retirar los estudiantes seleccionados de su habitación Habitación Lista de Habitaciones Habitaciones Desde Habitación del Estudiante La habitación ha sido asignada al estudiante seleccionado. Los estudiantes fueron retirados de su habitación. El estudiante fue retirado de la habitación. 