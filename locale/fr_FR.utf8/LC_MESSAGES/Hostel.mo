��          �      l      �     �     �  	   �                    #     +     8     A     H  (   a     �  	   �     �     �     �  .   �  3   �  &     �  >     �  	   �  
   �  	   �     �       
        (     :     K     S  3   q     �     �     �     �     �  8   �  ?     (   ^                               	                                      
                             All Buildings Building Buildings Capacity Hostel Include Students My Room New Building New Room Remove Remove Inactive Students Remove selected students from their room Room Room List Rooms Since Student's Room The room was assigned to the selected student. The selected students were removed from their room. The student was removed from the room. Project-Id-Version: Hostel module for RosarioSIS
PO-Revision-Date: 2024-04-22 17:51+0200
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-SearchPath-0: .
 Tous les bâtiments Bâtiment Bâtiments Capacité Internat Inclure les élèves Ma chambre Nouveau bâtiment Nouvelle chambre Retirer Retirer les élèves inactifs Retirer les élèves sélectionnés de leur chambre Chambre Liste des chambres Chambres Depuis Chambre de l'élève La chambre a été assignée à l'élève sélectionné. Les élèves sélectionnés ont été retirés de leur chambre. L'élève a été retiré de la chambre. 