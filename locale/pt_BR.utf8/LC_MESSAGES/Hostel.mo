��          �      l      �     �     �  	   �                    #     +     8     A     H  (   a     �  	   �     �     �     �  .   �  3   �  &     �  >     �  	   �     �  
   �  
   
       
   $     /     <     H     P  +   h     �     �     �     �     �  -   �  7   �     0                               	                                      
                             All Buildings Building Buildings Capacity Hostel Include Students My Room New Building New Room Remove Remove Inactive Students Remove selected students from their room Room Room List Rooms Since Student's Room The room was assigned to the selected student. The selected students were removed from their room. The student was removed from the room. Project-Id-Version: Hostel module for RosarioSIS
PO-Revision-Date: 2024-04-22 17:51+0200
Last-Translator: Emerson Barros
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: ;dgettext:2;dngettext:2
X-Poedit-SearchPath-0: .
 Todos os prédios Edifício Prédios Capacidade Alojamento Incluir alunos Meu quarto Novo prédio Novo quarto Remover Remover alunos inativos Remover alunos selecionados de seus quartos Quarto Lista de quartos Quartos Desde Quarto do aluno O quarto foi atribuído ao aluno selecionado. Os alunos selecionados foram removidos de seus quartos. O aluno foi removido da sala. 